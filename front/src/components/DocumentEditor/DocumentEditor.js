import _ from 'lodash';
import React from 'react';
import {getDifference} from './utils';

let position = 0
class TextArea extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.textAreaRef = React.createRef()
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    this.textAreaRef.current.selectionEnd = position
    return true
  }

  onChange = (event) => {
    position = event.target.selectionEnd
    const difference = getDifference(this.props.value, event.target.value, event.target.selectionEnd);
    if (!difference) {
      return;
    }

    switch (difference.operation) {
      case 'insert':
        this.props.onInsert(difference.position, difference.content);
        break;
      case 'delete':
        this.props.onDelete(difference.position, difference.content);
        break;
      case 'replace':
        this.props.onReplace(difference.position, difference.oldContent, difference.newContent);
        break;
      default:
        throw new Error('Unsupported operation');
    }
  };

  render() {
    return (
      <textarea
        {..._.omit(this.props, ['onInsert', 'onDelete', 'onReplace'])}
        onChange={this.onChange}
        ref={this.textAreaRef}
      />
    );
  }
}

export default TextArea;
