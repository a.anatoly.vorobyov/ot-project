package editor.operations;

import io.datakernel.ot.OTState;

public class EditorOTState implements OTState<EditorOperation> {
	private StringBuilder stringBuilder = new StringBuilder();

	@Override
	public void init() {
		stringBuilder = new StringBuilder();
	}

	@Override
	public void apply(EditorOperation op) {
		op.apply(stringBuilder);
	}

	public String getState() {
		return stringBuilder.toString();
	}
}
